(ns presentation.frontend.chat.core
  (:require [presentation.frontend.async.core :as async]
            [cuerdas.core :as str]
            [taoensso.timbre :as log]
            [reagent.core :as r]
            [reagent.ratom :as ratom :include-macros true]
            [cljs-time.core :as time]
            [cljs-uuid-utils.core :as uuid]))

; === dummy data

(defonce nick (r/atom "Zelda"))

(defonce channels (r/atom []))

(defonce active-channel-id (r/atom (-> @channels first :id)))

(defonce messages (r/atom {}))

; ===

(defn unread-messages [messages]
  (filter #(not (contains? % :read-timestamp)) messages))

(defn make-messages-read [messages]
  (let [timestamp (time/now)]
    (mapv #(assoc % :read-timestamp timestamp) messages)))

(defn add-channel! [channel-name]
  (let [channel {:id (uuid/make-random-uuid)
                 :name channel-name}]
    (swap! channels conj channel)))

(defn send-message! [text]
  (let [timestamp (time/now)
        id (uuid/make-random-uuid)
        message {:id id
                 :user @nick
                 :timestamp timestamp
                 :read-timestamp timestamp
                 :message text}]
    (swap! messages update @active-channel-id (comp vec conj) message)
    (async/send! :chat/new-message {:id id
                                    :channel-id @active-channel-id
                                    :user @nick
                                    :timestamp timestamp
                                    :message text})))

(defn make-messages-read! [channel-id]
  (swap! messages update channel-id make-messages-read))

; ===

(defn make-class [classes]
  (str/join " " classes))

(defn make-key [& fragments]
  (keyword (str/join "-" fragments)))

(defn channel-button
  ([name]
    (channel-button {} name))
  ([{:keys [active? unread? on-click]} name]
    [:a.list-group-item {:class    (make-class [(when active? "active")])
                         :href     "#"
                         :on-click (fn [e]
                                     (on-click)
                                     (.preventDefault e))}
       (when unread?
         [:span.label.label-default.label-pill.pull-right
           unread?])
       name]))

(defn add-channel-input []
  (let [channel-name (r/atom "")]
    (fn []
      [:div.row
        [:div.col-lg-12
          [:div.input-group
            [:input.form-control {:placeholder "Add channel..."
                                  :value @channel-name
                                  :on-change (fn [e]
                                               (reset! channel-name (.. e -target -value))
                                               (.preventDefault e))}]
            [:div.input-group-btn
              [:button.btn.btn-primary {:on-click (fn [e]
                                                    (add-channel! @channel-name)
                                                    (reset! channel-name "")
                                                    (.preventDefault e))}
                "+"]]]]])))

(defn channel-list []
  (let []
    (fn []
      [:div.row {:style {:height "100%"}}
        [:div.col-lg-12
          [:list-group
            (doall
              (for [{:keys [id name]} @channels]
                (let [unread-messages (count (unread-messages (get @messages id)))]
                  ; (log/debug "id=" id "; name=" name)
                  ^{:key (make-key "channel" id)}
                  [channel-button {:active? (= id @active-channel-id)
                                   :unread? (when (> unread-messages 0)
                                              unread-messages)
                                   :on-click #(reset! active-channel-id id)}
                    name])))
             [:div.list-group-time.add-channel
               [add-channel-input]]]]])))

(defn message-component [{:keys [user message]}]
  [:div
    [:b user]
    [:p message]])

(defn channel-messages []
  (let [channel-messages (ratom/reaction ;(sort-by :timestamp
                                                  (get @messages @active-channel-id)
                                                  ;)
                                         )
        last-message (r/atom nil)
        new-message?  (ratom/reaction
                       (let [newest-message (last @channel-messages)
                             new-message? (not= newest-message @last-message)]
                         (log/debug "NEW MESAGE!")
                         (reset! last-message newest-message)
                         new-message?))]
    (r/create-class
     {:component-did-update (fn [component]
                              (let [node (r/dom-node component)
                                    card-node (.querySelector node ".card")]
                                (when @new-message?
                                 (set! (.-scrollTop card-node) (.-scrollHeight card-node)))))
      :reagent-render
        (fn []
          (when-let [channel-messages (not-empty (unread-messages @channel-messages))]
            (log/debug "UNREAD MESSAGES" channel-messages)
            (make-messages-read! @active-channel-id))
          [:div.row {:style {:flex-grow "1"}}
            [:div.col-lg-12
              [:div.card {:style {:height "400px" :overflow-y "scroll"}}
                [:div.card-block
                  (doall (for [message @channel-messages]
                           ^{:key (make-key "message" (:id message))}
                           [message-component message]))]]]])})))

(defn message-input []
  (let [message (r/atom "")]
    (fn []
      [:div.row
        [:div.col-lg-12
          [:div.input-group
            [:input.form-control {:placeholder "Reply..."
                                  :value @message
                                  :on-change (fn [e]
                                               (reset! message (.. e -target -value))
                                               (.preventDefault e))}]
            [:div.input-group-btn
              [:button.btn.btn-primary {:on-click (fn [e]
                                                    (send-message! @message)
                                                    (reset! message "")
                                                    (.preventDefault e))}
                "Send"]]]]])))

(defn username-input []
  [:div.row
    [:div.col-lg-12
      [:input.form-control {:placeholder "Your nickname..."
                            :value @nick
                            :on-change (fn [e]
                                         (reset! nick (.. e -target -value))
                                         (.preventDefault e))}]]])

(defn chat-component []
  (let []
    (fn []
      [:div.container-fluid {:style {:height "100%"}}
        [:div.row {:style {:height "100%"}}
          [:div.col-lg-4
            [username-input]
           [:br]
            [channel-list]]

          [:div.col-lg-8
            [:div.container-fluid {:style {:height "400px"
                                           :display "flex"
                                           :flex-direction "column"}}
              [channel-messages]
              [message-input]]]]])))

(defmethod async/async-handler :chat/update-channels
  [event {:keys [channel-id] :as payload}]

  (reset! channels (:channels payload)))


(defmethod async/async-handler :chat/new-message
  [event {:keys [channel-id] :as payload}]

  (let [message (dissoc payload :channel-id)]
    (log/debug "Got new message: " message)
    (log/debug "Result0 " @messages)
    (log/debug "Result1 " (update @messages channel-id (comp vec conj) message))
    (log/debug
     "Result2 "
     (swap! messages update channel-id (comp vec conj) message))))

(js/setTimeout (fn [] (async/send! :chat/update-channels)) 500)
