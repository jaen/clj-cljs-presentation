(ns presentation.backend.async.core
  (:require [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.immutant :as sente-immutant]
            [taoensso.timbre :as log]
            [taoensso.sente.packers.transit :as sente-transit]
            [clojure.core.match :refer [match]]
            [cuerdas.core :as str]
            [clj-time.core :as time]
            [clj-uuid :as uuid]
            [presentation.common.utils.transit :as transit-utils]
            [presentation.backend.chat.core :as chat]))

;; ===

(declare send!)

(defmulti async-handler (fn [_ event] (:event event)))

(defmethod async-handler :default
  [state request]

  (log/debug "Unhandled event:" (dissoc request :ev-msg)))

;(defn make-sente-routes
;  []
;
;  [["sente" {:get  :async/handshake-fn
;             :post :async/ajax-post-fn}]])

(defn- make-sente-handlers
  [sente-socket]

  (let [handshake-fn    (:ajax-get-or-ws-handshake-fn sente-socket)
        ajax-post-fn    (:ajax-post-fn sente-socket)]
    {:async/handshake-fn handshake-fn
     :async/ajax-post-fn ajax-post-fn}))

(defn- base-handler
  [state {:as ev-msg :keys [uid client-id id ?data ?reply-fn]}]

  (let [{:keys [payload message-id]} ?data
        request {:ev-msg ev-msg
                 :event id
                 :client-id client-id
                 :user-id uid
                 :message-id message-id
                 :payload payload}]
    (async-handler state request)))

(defn make-handle-receive [state]
  (fn [{:keys [event] :as ev-msg}]

    (log/debug "Event:" event)
    (match event
           [:chsk/handshake  _]              (log/debug "Got handshake!")
           [:chsk/state {:first-open? true}] (log/debug "Channel socket successfully established!")
           [:chsk/state new-state]           (log/debug "Chsk state change:" new-state)
           :else                             (base-handler state ev-msg))))

(defn start-async! []
  (log/debug "Starting async.")
  (let [user-id-fn      (fn [request]
                          (let [client-id (:client-id request)]
                              client-id))
        sente-packer  (sente-transit/->TransitPacker
                         :json
                         {:handlers transit-utils/transit-write-handlers}
                         {:handlers transit-utils/transit-read-handlers})
        sente-socket  (sente/make-channel-socket!
                        sente-immutant/immutant-adapter
                        {:user-id-fn user-id-fn
                         :packer sente-packer})
        receive-chan    (:ch-recv sente-socket)
        sente-send!     (:send-fn sente-socket)
        send!-fn        (fn [uuid event payload]
                          (let [payload {:payload payload}]
                            (log/debug "sending payload:" payload)
                            (sente-send! uuid [event payload])))
        connected-uids  (:connected-uids sente-socket)
        sente-handlers  (make-sente-handlers sente-socket)
        handle-receive  (make-handle-receive {:send! send!-fn
                                              :uids connected-uids})
        sente-router    (sente/start-chsk-router! receive-chan handle-receive)]
    (log/debug "Started async.")
      {:sente-socket sente-socket
       :handlers sente-handlers
       :router sente-router
       ; :receive-chan receive-chan
       :send! sente-send!
       :uids connected-uids}))

(defn stop-async! [async-state]
  (log/debug "Stopping async.")
  (when-let [sente-router-stop! (:router async-state)]
    (log/info "Stopping async.")
    (sente-router-stop!)))

(defmethod async-handler :test/debug
  [state {:keys [payload]}]

  (log/debug "Received debug message: " payload))

(defmethod async-handler :chat/new-message
  [{:keys [send! uids]} {:keys [user-id payload]}]

  (log/debug "New chat message: " payload)
  (let [current-uids (:any @uids)]
    (doseq [uid current-uids]
      (when (not= uid user-id)
        (log/debug "Sending message to: " uid)
        (send! uid :chat/new-message payload)))))

(defmethod async-handler :chat/update-channels
  [{:keys [send!]} {:keys [user-id]}]

  (send! user-id :chat/update-channels {:channels (chat/get-channels)}))

