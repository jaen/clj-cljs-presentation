(ns presentation.frontend.application
  (:require-macros [reagent.ratom :as ratom]
                   [cljs.core.async.macros :as async-macros :refer [go go-loop]])
  (:require [presentation.frontend.chat.core :as chat]

            [reagent.core :as reagent]
            [cuerdas.core :as str]
            [cljs-uuid-utils.core :as uuid]
            [cljs.core.async :as async :refer [<! >!]]
            [hodgepodge.core :as storage]
            [taoensso.timbre :as log]
            [goog.events :as gevents])
  (:import [goog.events EventType]))

(def presenter-mode? (reagent/atom false))
(def presentation-storage-key :presentation-storage-key)

(defn get-storage-state! []
  (let [state (get storage/local-storage presentation-storage-key)]
    ;(log/info "Synchronising state" state)
    state))

(def initial-state
  {:history [[:first-slide]]
   :counter 0})

(def state (reagent/atom (or (get-storage-state!)
                             initial-state)))

(defn reset-state! []
  (reset! state initial-state))

(add-watch state :state-storage-watch (fn [_ _ _ new-val]
                                        (assoc! storage/local-storage presentation-storage-key new-val)))

; (gevents/unlisten storage/local-storage goog.events.EventType.STORAGE)
(when-not false                                             ;(exists? state-listener-guard)
  ; (def state-listener-guard true)
  (gevents/listen js/window goog.events.EventType.STORAGE
    (fn [e]
      (when (= (str presentation-storage-key) (.. e -event_ -key))
        (reset! state (get-storage-state!))))))

(defonce navigation-chan (async/chan))

; (defonce counter (reagent/atom 0))

(defn transform-for [descriptor]
  (let [{[x y z] :position [rotx roty rotz] :rotation scale :scale} descriptor]
    (str "translate3d(" x "px, " y "px, " (+ z 500) "px) "
         "rotateX(" rotx "deg) rotateY(" roty "deg) rotateZ(" rotz "deg) "
         "scale(" scale ")")))

(def slide-history
  (ratom/reaction (:history @state)))

(def current-slide-path
  (ratom/reaction (peek @slide-history)))

(declare slides)

(def current-slide
  (ratom/reaction (get-in slides @current-slide-path)))

(defn move-to-next-slide! []
  (when-let [next-slide (:next @current-slide)]
    (swap! state update-in [:history] conj [next-slide])))

(defn move-to-previous-slide! []
  (when (> (count @slide-history) 1)
    (swap! state update-in [:history] pop)))

(defn move-to-slide-name! [slide-name]
  (swap! state update-in [:history] conj [slide-name]))

(def first-slide-descriptor
  {:name      :first-slide
   :position  [-1000 -1500 0]
   :rotation  [0 0 0]
   :scale     1
   :next      :second-slide
   :component (fn []
                (let [counter (reagent/wrap (or (:counter @state) 0) swap! state assoc :counter)]
                  [:div.slide-content
                    [:div "hello " @counter]
                    [:div [:a {:on-click (fn [e]
                                           (swap! counter inc)
                                           (.preventDefault e))}
                           "Click me!!!"]]]))})

(def second-slide-descriptor
  {:name :second-slide
   :position [0 -1500 0]
   :rotation [0 0 0]
   :scale 1
   :next :third-slide
   :component (fn []
                [:div.slide-content
                  [chat/chat-component]])})

(def third-slide-descriptor
  {:name :third-slide
   :position [0 -1500 -500]
   :rotation [0 180 0]
   :scale 1
   :component (fn []
                [:div.slide-content
                 [:div "I'm the third slide"]])})

(def slides
  {:first-slide first-slide-descriptor
   :second-slide second-slide-descriptor
   :third-slide third-slide-descriptor})

(defn slide-style [slide-descriptor]
  {:transform (str "translate(-50%, -50%) " (transform-for slide-descriptor))})

(defn slide-component [slide-descriptor]
  [:div.slide {:style    (slide-style slide-descriptor)
               :on-click #() #_(fn [e]
                           (.log js/console "WELP")
                           (move-to-slide-name! (:name slide-descriptor))
                           (.preventDefault e))}
    [(:component slide-descriptor)]])

(def quadratic-easing
  "cubic-bezier(0.455, 0.03, 0.515, 0.955)")

;(defonce camera
;  (reagent/atom {:position [1000 1500 -1000]
;                 :rotation [0 0 0]
;                 :scale    1
;                 :speed    1000
;                 :easing   quadratic-easing}))

(defn position-for-camera [position]
  (let [[x y z] position]
    [(- x) (- y) (+ (- z) -1000)]))

(def camera
  (ratom/reaction (assoc
                    (update (select-keys @current-slide [:position :rotation :scale]) :position position-for-camera)
                    :speed 1000
                    :easing quadratic-easing)))

;(defn random-parens [base]
;  (iterate (fn [_]
;             (let [type (if (= (mod (rand-int 1337) 2) 0) :left :right)
;                   descriptor {:position [(- (rand-int 2500) 1250)
;                                          (- (rand-int 2500) 1250)
;                                          (- (rand-int 20000) 10000)]
;                               :rotation [0 0 0]
;                               :scale    1}
;                   style {:transform (transform-for descriptor)
;                          :color (if (= type :left) "#63B132" "#5881D8")}
;                   text (if (= type :left) "(" ")")]
;               ^{:key (uuid/uuid-string (uuid/make-random-uuid))}
;               [:div.paren {:style style} text]))
;           [:div.paren ""]))
;
;(defn parens-component []
;  (let [base  first-slide-descriptor
;        style (let [{[x y z] :position [rotx roty rotz] :rotation scale :scale} base]
;                {:transform (str "translate3d(" x "px, " y "px, " (+ z 500) "px) "
;                                 "rotateX(" rotx "deg) rotateY(" roty "deg) rotateZ(" rotz "deg) "
;                                 "scale(" scale ")")})]
;    [:div.parens {:style style}
;      (take 1000 (random-parens base))]))

(defn slide-container-style [camera]
  (let [{speed :speed easing :easing} camera]
    {:transform-origin "left top 0px"
     :transition (str "all " speed "ms " easing " 0ms")
     :transform (transform-for camera)}))

(defn scale-for-viewport [{:keys [width height] :as x}]
  (log/debug "X " x)
  (let [vscale (/ width 1920)
        hscale (/ height 1080)
        scale (min vscale hscale)]
    scale))

(defn slides-component [viewport-size]
  (let [;scale  (scale-for-viewport @viewport-size)
        slides (for [[name slide-descriptor] slides]
                 ^{:key name}
                 [slide-component slide-descriptor])]
    ;(log/debug "SCALE " scale)
    (vec (concat [:div.slide-container {:style (slide-container-style @camera) #_(slide-container-style (assoc @camera :scale scale))}
                  #_[parens-component]]
                 slides))))

(def ResizeSensor js/ResizeSensor)

(defn resize-watch-component
  "Creates a component that detects if it's contents are being resized.
   First argument can optionally be a map of following options:

     * :on-resize - a callback that's called when the "
  [first & _]
  (let [options   (if (map? first) first {})
        _ (log/debug "CLASS " (:class options []))
        class     (str/join " " (filter some? (:class options [])))
        on-resize (:on-resize options (fn [_]))]
    (reagent/create-class
     {:component-did-mount (fn [component] (let [node (reagent/dom-node component)]
                                             (ResizeSensor. node (fn [] (on-resize node)))
                                             (on-resize node))) ;; INFO: call once to initialise the placeholder size
      :reagent-render      (fn [& [first & rest :as args]]
                             (let [content (if (map? first) rest args)
                                   options   (if (map? first) first {})
                                   class     (str/join " " (filter some? (:class options [])))]
                               (vec (cons :div (cons {:class class} content)))))})))

(defn get-dimensions [node]
  (let [rect   (.getBoundingClientRect node)
        width  (.-width rect)
        height (.-height rect)] ;
    (loop [node node
           top  0
           left 0]
      (if node
        #_(log/debug "node:" node)
        (let [rect (.getBoundingClientRect node)]
          (recur (.-offsetParent node) (+ top (.-offsetTop node) #_(.-top rect)) (+ left (.-offsetLeft node) #_(.-left rect))))
        {:top top :left left :width width :height height}))))

(defonce
 viewport-size (reagent/atom nil))

(defn presentation-container []
  (let [; viewport-size (reagent/atom nil)]
        ]
;     (log/debug "SCALE: " @viewport-size)
    (reagent/create-class
      {
        :reagent-render
        (fn []
          (log/debug "SCALE: " @viewport-size)
          [resize-watch-component {:class ["presentation-container" (when @presenter-mode? "presenter-mode")]
                                   :on-resize (fn [node]
                                                (log/debug "Set viewport size: " (reset! viewport-size (get-dimensions node))))}
              [:div.presentation {:style {:transform-style "preserve-3d"
                                          :transform (str "perspective(1460.08px) scale("
                                                          (scale-for-viewport @viewport-size)
                                                          ")")}}
               [slides-component viewport-size]]])})))

(defn testp []
  (log/debug "PRESENTER " @presenter-mode?)
  (if @presenter-mode?
    [:div.presenter-mode-container
     [:div.notes
      "notes go here"]
     presentation-container]
    presentation-container))

(defn application-component []
  [:div.lel [testp]])

(defn move [x y z]
  (swap! camera assoc :position [x y z]))

(defmulti handle-nav-message (fn [[message & _]] message))

(defmethod handle-nav-message :next-slide [_]
  (move-to-next-slide!))

(defmethod handle-nav-message :previous-slide [_]
  (move-to-previous-slide!))

(go-loop []
  (let [nav-message (<! navigation-chan)]
    (handle-nav-message nav-message)
    (recur)))

(defn next-slide! []
  (async/put! navigation-chan [:next-slide]))

(defn previous-slide! []
  (async/put! navigation-chan [:previous-slide]))

(defn show-presenter-mode! []
  (log/debug "Toggling presenter mode.")
  (swap! presenter-mode? not))