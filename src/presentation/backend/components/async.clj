(ns presentation.backend.components.async
  (:require [com.stuartsierra.component :as component]
            [presentation.backend.async.core :as async]))

(defrecord Async [async-state]
  component/Lifecycle
    (start [component]
      (let [{:keys [handlers send! uids] :as async-state} (async/start-async!)]
        (assoc component :async-state async-state
                         :handlers handlers
                         :send! send!
                         :uids uids)))

    (stop [component]
      (when async-state
        (async/stop-async! async-state)
        (dissoc component :async-state :handlers :send :uids))))

(defn new-async
  []

  (map->Async {:host "localhost:3000"}))
