(ns presentation.backend.core
  (:require [system.components
             [immutant-web :as immutant]]
            [com.stuartsierra.component :as component]
            [bidi.bidi :as bidi]
            [bidi.ring :as bidi-ring]
            [presentation.backend.components.async :as async]
            [taoensso.timbre :as log]
            [ring.middleware.params :as ring-params]
            [ring.middleware.keyword-params :as ring-keyword-params]))

(defn dev-handler [req]
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    "test"})


(defrecord Application [async app]
  component/Lifecycle
  (start [component]
    (let [{sente-handlers :handlers} async
          routes  ["/" [["test" :test]
                        ["sente" {:get  :async/handshake-fn
                                  :post :async/ajax-post-fn}]]]
          handlers (merge {:test dev-handler}
                          sente-handlers)
          bidi-handler (bidi-ring/make-handler routes handlers)
          ring-handler (-> bidi-handler
                           ring-keyword-params/wrap-keyword-params
                           ring-params/wrap-params)]
      (assoc component :app ring-handler)))

  (stop [component]
    (assoc component :app nil)))

(defn make-application []
  (map->Application {}))

(defn dev-system []
  (component/system-map
    :async       (async/new-async)
    :application (component/using (make-application) {:async :async})
    :web         (component/using (immutant/new-web-server 3000) {:handler :application})))
