(ns presentation.backend.chat.core
  (:require [clj-time.core :as time]
            [clj-uuid :as uuid]))

(defonce channels (atom [{:id (uuid/v1)
                            :name "Channel 1"}
                           {:id (uuid/v1)
                            :name "Channel 2"}
                           {:id (uuid/v1)
                            :name "Channel 3"}]))

(defn make-test-messages [{:keys [id name]}]
  [id [{:id (uuid/v1)
        :user "a"
        :timestamp (time/plus (time/now) (time/minutes 1))
        :message (str name "test")}
       {:id (uuid/v1)
        :user "b"
        :timestamp (time/plus (time/now) (time/minutes 2))
        :message (str name "test2")}
       {:id (uuid/v1)
        :user "c"
        :timestamp (time/plus (time/now) (time/minutes 3))
        :message (str name "test3")}]])

(defonce messages (atom (into {} (map make-test-messages @channels))))

(defn get-channels []
  (vec @channels))

(defn add-channel! [channel]
  (swap! vec conj channels channel))

(defn get-messages [channel-id]
  (vec (get @messages channel-id)))

(defn add-message! [channel-id message]
  (swap! messages update channel-id conj message))