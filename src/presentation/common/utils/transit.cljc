(ns presentation.common.utils.transit
  (:require [cognitect.transit :as transit]
   #?@(:clj [[clj-time.format :as time-format]]
       :cljs [[cljs-time.format :as time-format]
              [datascript.transit :as dt]])
            [presentation.common.utils.time :as time-utils])
  #?(:clj  (:import (org.joda.time DateTime)
                    (java.io ByteArrayInputStream ByteArrayOutputStream))
     :cljs (:import (goog.date Date DateTime UtcDateTime))))

(def ^:private clj-time-writer
  (transit/write-handler
   (constantly "m")
   (fn [v] v)
   (fn [v] (time-utils/to-str v))))

(def ^:private clj-time-reader
  (transit/read-handler
   (fn [v] (time-utils/from-str v))))

(def transit-write-handlers
  #?(:clj  {DateTime clj-time-writer}
     :cljs (merge {goog.date.Date clj-time-writer
                   goog.date.DateTime clj-time-writer
                   goog.date.UtcDateTime clj-time-writer}
                  dt/write-handlers)))

(def transit-read-handlers
  #?(:clj  {"m"                clj-time-reader}
     :cljs (merge {"m"                clj-time-reader}
                  dt/read-handlers)))

#?(:clj (defn to-str [obj]
          (let [string-writer  (ByteArrayOutputStream.)
                transit-writer (transit/writer string-writer :json {:handlers transit-write-handlers})]
            (transit/write transit-writer obj)
            (.toString string-writer)))
   :cljs (defn to-str [obj]
           (let [writer (transit/writer :json {:handlers transit-write-handlers})]
             (transit/write writer obj))))

#?(:clj (defn from-str [str]
          (let [string-reader  (ByteArrayInputStream. (.getBytes str))
                transit-reader (transit/reader string-reader :json {:handlers transit-read-handlers})]
            (transit/read transit-reader)))
   :cljs (defn from-str [str]
           (let [reader (transit/reader :json {:handlers transit-read-handlers})]
             (transit/read reader str))))