(ns presentation.frontend.async.core
  (:require
    [cljs.core.match :refer-macros [match]]

    [taoensso.sente :as sente]
    [cljs-uuid-utils.core :as uuid]

    [presentation.common.utils.transit :as transit-utils]
    [taoensso.sente.packers.transit :as sente-transit]
    [taoensso.timbre :as log]
    [taoensso.encore :as encore]
    [cljs.core.async :as async]))

(declare disconnect!)
(declare connect!)

(def connected? (atom false))

(defmulti async-handler (fn [event payload] event))

(defmethod async-handler :default ; Fallback
  [event payload]
  (log/debug "Unhandled event:" event payload))

(defn- base-handler [{:as ev-msg :keys [?data]}]
  (log/debug "Event:" ?data)
  (let [[event {:keys [payload]}] ?data]
    (log/debug "Dispatching event:" event payload)
    (async-handler event payload)))


(defn- handle-receive [{:keys [event send-fn] :as ev-msg}]
  (let [[id data :as ev] event]
    (log/debug "Event:" event)
    (log/debug "Event id:" id )
    (match event
      [:chsk/recv _]
        (do
          (log/debug "Push event from server")
          (base-handler ev-msg))
      _
        (log/debug "Unmatched event:" ev))))

(defonce sente-async
  (atom nil))

(defn connect! []
  (when-not @sente-async
    (let [handler handle-receive
          packer  (sente-transit/->TransitPacker
                    :json
                    {:handlers transit-utils/transit-write-handlers}
                    {:handlers transit-utils/transit-read-handlers})
          remote-host "himitsu"
          sente (sente/make-channel-socket!
                   "/sente"
                   {:type   :auto
                    :packer packer
                    :chsk-url-fn (constantly (str "ws://" remote-host ":3000/sente"))})
          sente-receive-chan (:ch-recv sente)
          sente-send-fn      (:send-fn sente)
          router (sente/start-chsk-router! sente-receive-chan handler)
          sente-data {:sente-receive-chan sente-receive-chan
                      :send!              sente-send-fn
                      :stop-router!       router}]
      (reset! sente-async sente-data))))

(defn disconnect! []
  (when @sente-async
    (let [{:keys [stop-router! sente-receive-chan]} @sente-async]
      (async/close! sente-receive-chan)
      (stop-router!)
      (reset! sente-async nil))))

(defn send! [event-name & [payload]]
  (let [message-id (uuid/make-random-uuid)
        event [event-name {:message-id message-id :payload (or payload {})}]]
    ((:send! @sente-async) event)
    message-id))

(defn test-send! []
  (send! :test/debug {:top "kek"}))
