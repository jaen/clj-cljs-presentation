(ns presentation.frontend.core
  (:require [devtools.core :as devtools]
            [reagent.core :as reagent]
            [taoensso.timbre :as log]
            [goog.events :as gevents]

            [presentation.frontend.application :as app]
            [presentation.frontend.utils.log :as utils-log]
            [presentation.frontend.async.core :as async])
  (:import [goog.events EventType]))

(defn install-devtools! []
  (devtools/set-pref! :install-sanity-hints true) ; this is optional
  (devtools/install!))

(defn mount-root! []
  (reagent/render [app/application-component] (. js/document (querySelector ".app-container"))))

(defn handle-key [key]
  (case key
    39 (app/next-slide!)
    37 (app/previous-slide!)
    80 (app/show-presenter-mode!)
       nil))

(defn setup-key-handler! []
  (gevents/listen js/window goog.events.EventType.KEYDOWN
    (fn [e]
      (let [key (.-keyCode e)]
        (log/info "Clicked: " key)
        (when (handle-key key)
          (.preventDefault e))))))

(defn app-init! []
  (install-devtools!)
  (async/connect!)
  (when-let [appender (utils-log/make-console-appender)]
    (log/merge-config! {:appenders {:console appender}}))
  (setup-key-handler!)
  ; (log/debug "starting app")
  (mount-root!)
  #_(history/setup-history! (.-body js/document)))

(defn app-reload! []
  ; (log/debug "reloading app")
  ; (async/disconnect!)
  ; (async/connect!)
  (mount-root!))
