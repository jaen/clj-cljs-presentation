(defn read-dependencies! []
  (let [deps (read-string (slurp "resources/deps.edn"))]
    deps))

(set-env!
  :source-paths #{"src" "dev"}
  :resource-paths #{"resources"}
  :dependencies (read-dependencies!)
  :exclusions '[org.clojure/clojure org.clojure/clojurescript])

(require '[pandeiro.boot-http :as bh]
         '[adzerk.boot-reload :as br]
         '[adzerk.boot-cljs :as bc]
         '[adzerk.boot-cljs-repl :as bcr]
         ;'[jeluard.boot-notify :as bn]
         '[system.boot :as bs]
         '[boot.util :as util]

         '[middleman :as bm]
         '[dev-handler]
         '[system-boot :as sb]

         '[presentation.backend.core])

(task-options!
  pom {:project 'clj-cljs-presentation
       :version "0.1.0-SNAPSHOT"
       :url "https://gitlab.com/jaen/clj-cljs-presentation"})

(defn- generate-lein-project-file! [& {:keys [keep-project] :or {:keep-project true}}]
  (require 'clojure.java.io)
  (let [pfile ((resolve 'clojure.java.io/file) "project.clj")
        ; Only works when pom options are set using task-options!
        {:keys [project version]} (:task-options (meta #'boot.task.built-in/pom))
        prop #(when-let [x (get-env %2)] [%1 x])
        head (list* 'defproject (or project 'boot-project) (or version "0.0.0-SNAPSHOT")
               (concat
                 (prop :url :url)
                 (prop :license :license)
                 (prop :description :description)
                 [:dependencies (get-env :dependencies)
                  :source-paths (vec (concat (get-env :source-paths)
                                             (get-env :resource-paths)))]))
        proj (pp-str head)]
      (if-not keep-project (.deleteOnExit pfile))
      (spit pfile proj)))

(deftask lein-generate
  "Generate a leiningen `project.clj` file.
   This task generates a leiningen `project.clj` file based on the boot
   environment configuration, including project name and version (generated
   if not present), dependencies, and source paths. Additional keys may be added
   to the generated `project.clj` file by specifying a `:lein` key in the boot
   environment whose value is a map of keys-value pairs to add to `project.clj`."
  []

  (with-pre-wrap fileset
    (util/info "Regenerating project clj...\n")
    (generate-lein-project-file! :keep-project true)
    fileset))

(deftask update-deps
  []

  (with-pre-wrap fileset
    (util/info "Dependencies changed, updating...\n")
    (set-env! :dependencies (fn [deps] (read-dependencies!)))
    fileset))

(deftask dev
  "Starts the dev env"
  []
  (comp
    (watch)
    (update-deps)
    (lein-generate)
    (bh/serve :handler 'dev-handler/my-dir-handler :port 8080)
    (bm/middleman :out "public")
    (br/reload :ip "0.0.0.0" :ws-host "himitsu" :on-jsload 'presentation.frontend.core/app-reload!)
    (bcr/cljs-repl :ip "0.0.0.0" :ws-host "himitsu")
    (bc/cljs :source-map true
             :optimizations :none
             :ids #{"public/assets/javascripts/application"}
             :compiler-options {:compiler-stats true})
    ;(bn/notify)
    (sb/system :sys #'presentation.backend.core/dev-system :auto-start true :hot-reload true :regexes [#"^presentation/backend/"])))

(defn cljs-repl! []
  (bcr/start-repl))
