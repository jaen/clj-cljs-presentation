(ns presentation.common.utils.time
  (:require #?@(:clj [[clj-time.format :as time-format]]
                :cljs [[cljs-time.format :as time-format]])))

(def app-time-formatter
  (time-format/formatters :date-time))

(defn to-str [date-time]
  (time-format/unparse app-time-formatter date-time))

(defn from-str [date-time-str]
  (time-format/parse app-time-formatter date-time-str))